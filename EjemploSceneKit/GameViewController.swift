import UIKit
import SceneKit
import Foundation
import AVFoundation
import QuartzCore

let π : Float = 3.141595



class GameViewController: UIViewController {
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // create a new scene
        let scene : SCNScene = SCNScene()
        
        // configure the view
        let scnView = self.view as SCNView
        scnView.scene = scene
        scnView.allowsCameraControl = true
        scnView.backgroundColor = UIColor.blackColor()
        
        
        
        // create and add a camera to the scene
        let cameraNode = SCNNode()
        cameraNode.camera = SCNCamera()
        cameraNode.position = SCNVector3(x: 0, y: 3, z: 8)
        
        scene.rootNode.addChildNode(cameraNode)
        
        // create and add a light to the scene
        let lightNode = SCNNode()
        lightNode.light = SCNLight()
        lightNode.light!.type = SCNLightTypeOmni
        lightNode.position = SCNVector3(x: 0, y: 10, z: 10)
        scene.rootNode.addChildNode(lightNode)
        
        // create and add an ambient light to the scene
        let ambientLightNode = SCNNode()
        ambientLightNode.light = SCNLight()
        ambientLightNode.light!.type = SCNLightTypeAmbient
        ambientLightNode.light!.color = UIColor.darkGrayColor()
        scene.rootNode.addChildNode(ambientLightNode)
        
        // ** Cubo
        /*
        
        Create a cube,
        set its position
        and
        add it to the scene
        
        
        Nota: chamferRadius es para redondear las esquinas del cubo
        */
        let cubo = SCNNode()
        cubo.geometry = SCNBox(width: 1.5, height: 1.5, length: 1.5, chamferRadius: 0)
        cubo.position = SCNVector3(x: 0, y: 0, z: 0)
        scene.rootNode.addChildNode(cubo)
        
        // use a texture for the cube
        let texturaCubo = SCNMaterial()
        texturaCubo.diffuse.contents = UIImage(named: "logoTyfAccSoft.png")
        cubo.geometry?.firstMaterial = texturaCubo
        
        // A basic action for an animation
        cubo.runAction(SCNAction.repeatActionForever(SCNAction.rotateByX(0, y: 1, z: 0, duration: 5)))
        
        // a cube that it is not a cube
        let cubo2 : SCNNode = cubo.clone() as SCNNode
        cubo2.position = SCNVector3(x: 2, y: 0, z: 0)
        cubo2.geometry = SCNTorus(ringRadius: 1, pipeRadius: 1)
        scene.rootNode.addChildNode(cubo2)
        
        
        
        
        
        
        
        // Tiro a la lata
        let lata = createLata()
        lata.position = (SCNVector3(x: -3, y: 0, z: 0))
        scene.rootNode.addChildNode(lata)
        
        // Add accessibility support
        lata.isAccessibilityElement = true
        lata.accessibilityLabel = "Una jodida lata"
        
        
        
        
        
        // add a tap gesture recognizer
        let tapGesture = UITapGestureRecognizer(target: self, action: "handleTap:")
        let gestureRecognizers = NSMutableArray()
        gestureRecognizers.addObject(tapGesture)
        if let existingGestureRecognizers = scnView.gestureRecognizers {
            gestureRecognizers.addObjectsFromArray(existingGestureRecognizers)
        }
        scnView.gestureRecognizers = gestureRecognizers
    }
    
    override func supportedInterfaceOrientations() -> Int {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return Int(UIInterfaceOrientationMask.AllButUpsideDown.toRaw())
        } else {
            return Int(UIInterfaceOrientationMask.All.toRaw())
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }
    
    override func shouldAutorotate() -> Bool {
        return true
    }
    
    
    // Tiro a la lata
    
    func createLata() -> SCNNode {
        let node = SCNNode()
        // Materials
        let metal = SCNMaterial()
        metal.diffuse.contents = UIColor.blueColor()
        metal.emission.contents = UIColor.cyanColor()
        let paper = SCNMaterial()
        paper.diffuse.contents = UIImage(named: "logoTyfAccSoft.png")
        
        // una lata
        var lata = SCNNode()
        lata.geometry = SCNCylinder(radius: 1, height: 3)
        lata.geometry?.firstMaterial = metal
        var lataPaper = SCNNode()
        lataPaper.geometry = SCNCylinder(radius: 1.01, height: 2.8)
        lataPaper.geometry?.firstMaterial = paper
        lata.addChildNode(lataPaper)
        node.addChildNode(lata)
        return node
    }
    
    // Gesture management
    
    
    func handleTap(gestureRecognize: UIGestureRecognizer) {
        // retrieve the SCNView
        let scnView = self.view as SCNView
        
        // check what nodes are tapped
        let p = gestureRecognize.locationInView(scnView)
        if let hitResults = scnView.hitTest(p, options: nil) {
            // check that we clicked on at least one object
            if hitResults.count <= 0 {
                playShot()
            } else {
                
                // retrieved the first clicked object
                let result: AnyObject! = hitResults[0]
                self.playSong()
                
                // para darle a toda la lata
                var touchedNode : SCNNode = result!.node
                if touchedNode.parentNode?.isKindOfClass(SCNNode) == true {
                    touchedNode = result!.node.parentNode!
                }
                
                (touchedNode.runAction(SCNAction.repeatAction(SCNAction.rotateByX(1, y: 0, z: 0, duration: 0.5), count: 50)))
            }
        }
    }
    
    
    // Sound management
    
    var audioPlayer = AVAudioPlayer()
    
    func playSong() {
        var song = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("song", ofType: "mp3")!)
        var error:NSError?
        audioPlayer = AVAudioPlayer(contentsOfURL: song, error: &error)
        audioPlayer.prepareToPlay()
        audioPlayer.play()
    }
    
    func playShot() {
        var song = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("shot", ofType: "mp3")!)
        var error:NSError?
        audioPlayer = AVAudioPlayer(contentsOfURL: song, error: &error)
        audioPlayer.prepareToPlay()
        audioPlayer.play()
    }
    
    
}
